# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

listaFornecedor = []

for i in range(0, 346):

    page_link = 'http://www.abre.org.br/cgi-local/wxis?IsisScript=search.xis&qtd=1&chavea=' + str(i*10) + '&qde=10&fmt=expresso_lx&base=&system=linux&acao=pesquisa&cliente=abre&base=abe&chave=TM%3DPESQ*%24*%24*%24&chavec=3451&x=30&y=9&pesq=A&mat=pesq'

    print(page_link)
    
    page_response = requests.get(page_link, timeout=5)

    page_content = BeautifulSoup(page_response.content, "html.parser")


    tables = page_content.find_all('table', {"bordercolor": "#666666"})

    #print(tables.count)
    # print(tables[6])
    print(len(tables))

    for table in tables:

        tds = table.find_all("td")

        # print("tds %d" % len(tds))

        nomeFornecedor = ""
        produto = ""
        regiao = ""
        cidade = ""
        telefone = ""
        email = ""
        site = ""
        fax = ""

        for i in range(0, len(tds)):

            # print(tds[i].text)

            result = tds[i].text.encode("utf-8")

            if result == " Associado" or result == "Fornecedor":
                nomeFornecedor = tds[i+1].text.encode("utf-8")
                # print(nomeFornecedor)
        
            if result == "Produto":
                produto = tds[i+1].text.encode("utf-8")
        
            if result == "Região":
                regiao = tds[i+1].text.encode("utf-8")

            if result == "Cidade/UF":
                cidade = tds[i+1].text.encode("utf-8")

            if result == "Fone":
                telefone = tds[i+1].text.encode("utf-8")

            if result == "E-mail":
                email = tds[i+1].text.encode("utf-8")

            if result == "End. Internet":
                site = tds[i+1].text.encode("utf-8")

            if result == "Fax":
                fax = tds[i+1].text.encode("utf-8")
        
        fornecedor = []
        fornecedor.append(nomeFornecedor)
        fornecedor.append(produto)
        fornecedor.append(regiao)
        fornecedor.append(cidade)
        fornecedor.append(telefone)
        fornecedor.append(fax)
        fornecedor.append(email)
        fornecedor.append(site)
        

        listaFornecedor.append(fornecedor)

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'fornecedores.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['Associado', 'Produto', 'Região', 'Cidade', 'Telefone', 'Fax', 'Email', 'Site'])
    
    for fornecedor in listaFornecedor:
        spamwriter.writerow(fornecedor)


        

    
# print("table %d" % len(tables))
