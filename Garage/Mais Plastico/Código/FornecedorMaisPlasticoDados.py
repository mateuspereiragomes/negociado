# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')


# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoInput = path + 'LinksCodigosFornecedoresMaisPlastico.csv'

codigos = []

with open(arquivoInput, 'r') as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    # for row in reader:
    #     print(row)

    codigos = list(reader)

csvFile.close()


listaFornecedor = []

i = -1
for codigo in codigos:

    # if i > 5: 
    #     break
    i = i + 1

    page_link = 'https://www.maisplastico.com.br/' + codigo[0]

    print (str(i) + '/' + str(len(codigos)) + ' ' + page_link)

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    #print(page_response)

    page_content = BeautifulSoup(page_response.content, "html.parser")

    # print(page_content)

    divs = page_content.find_all('div', {"class": "who margin-bottom-30"})

    fornecedor = []

    nomeFornecedor = ""
    endereco = "" 
    site = "" 
    telefone = "" 
    descricao = "" 

    for div in divs:

        nomeFornecedor = div.h2.text

        lis = div.find_all('li')

        for li in lis:
        
            if li.i['class'][1] == 'fa-home':
                endereco = li.text

            if li.i['class'][1] == 'fa-phone':
                telefone = li.text

            if li.i['class'][1] == 'fa-globe':
                site = li.text

    divs = page_content.find('div', {"id": "formulariocontato"})

    if divs.find_previous_sibling('div') != None:
        descricao = divs.find_previous_sibling('div').text

    fornecedor.append(codigo)
    fornecedor.append(nomeFornecedor)
    fornecedor.append(endereco)
    fornecedor.append(telefone)
    fornecedor.append(site)
    fornecedor.append(descricao)

    listaFornecedor.append(fornecedor)


arquivoOutput = path + 'DadosCodigosFornecedoresMaisPlastico.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    spamwriter.writerow(['Codigo', 'Fornecedor', 'Endereco', 'Telefone', 'Site', 'Descricao'])
    
    for fornecedor in listaFornecedor:

        spamwriter.writerow(fornecedor)