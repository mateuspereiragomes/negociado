# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import sys  
import time
from unicodedata import normalize
reload(sys)  
sys.setdefaultencoding('utf-8')

def getLinks(url, base):

    start = time.clock()

    page_content = ""

    try:

        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

        page_response = requests.get(url, headers=headers)

        page_content = BeautifulSoup(page_response.content.decode('utf-8', 'ignore'), "html.parser")
    
    except:
        print "Wrong"
        return []
  
    links = []
    for link in page_content.findAll('a', attrs={'href': re.compile(r'[/]([a-z]|[A-Z])\w+')}):
        # if 'cadastronacional.com.br' in link.get('href'): #and link.get('href') not in links:
        formated = link.get('href')
        
        if '//' in formated and base not in formated:
            continue
        if '//' in formated and base in formated:
            links.append(formated)
        if '//' not in formated and base not in formated:
            links.append('http://www.' + base + formated)
    
    print('getLinks:' + str(time.clock() - start))
    
    return links
   
    
base = 'jorsa.com.br'
links = ['http://www.jorsa.com.br']

startTotal = time.clock()
i = 0
while i < len(links):
    print(str(i) +  '/ ' + str(len(links)) + ' ' + links[i])
    start = time.clock()
    for link in getLinks(links[i],base):
        if link not in links:   
            links.append(link)
    i = i + 1
    print('includeLinks:' + str(time.clock() - start))
    if len(links) > 2070:
        break

print('Total:' + str(time.clock() - startTotal))



# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'Links.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['Links'])
    
    for link in links:
        spamwriter.writerow([link])
