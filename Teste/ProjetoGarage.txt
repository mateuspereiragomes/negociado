Resumo de Ideias para o Projeto 

FRENTE FORNECEDORES 

    ALGORITIMO - BIG DATA - MACHINE LEARNING - IA

    1-Captura de lista geral de Fornecedores de sites na WEB. 
    2-Captura de lista geral de produtos de sites da WEB. 
    3-Tratamento dos dados captados 
        - Junção em uma única planilha. 
        - Teste de sites válidos
        - Captura de palavras chave dos sites
            - Mapear todo o site 
            - Buscar palavras chave 
            - Armazenar palavras chave
            - Busca por palavras chave

    4-Fazer busca por nicho de mercado 
    5-Conferir empresas manualmente
        -Adicionar mais dados: CNPJ, endereços, telefones, email, catálogos
        -Buscar dados do CNPJ
        -Fazer busca de produtos no catálogo

    INSERÇÃO MANUAL

    1-Busca manual de novos Fornecedores
    2-Inserção de dados na plataforma Fornecedores
        -Nome.... endereco... CNPJ ... Catalogo 
    3-Insercao de produtos manual no sistema 
    
    PÁGINAS
        -FORNECEDORES
        -PRODUTOS
        -DISTRIBUIDORES (PF OU PJ)
        -CLIENTES (PF OU PJ)


FRENTE CLIENTES

    CNPJ
    1-Capturar dados do CNPJ
    2-Fazer busca por nicho de mercado, região 
    3-Fazer prospecção por email: 
        -Robo que envia apresentação da empresa 
    4-Fazer prospecção por telefone
    5-Gerenciamento de clientes


    POR TIPO MERCADO: RESTAURANTE, CAFETERIAS 
    1-Capturar informações por região 
    2-Tratar esses dados 
    3-Fazer prospecção por email 
    4-fazer prospecção por telefone
    5-Gerenciamento de clientes

    LANDING PAGE 
    0-Definir os dados que serão recebidos (email, telefone, informações da necessidade)
    1-Criar página web  (dá para buscar em criadores padrão, até o design)
    2-Configurar servirdor
    3-Configurar Domínio (não precisa inicialmente)
    4-Tratar os dados recebidos  

    REDES SOCIAIS (DIVULGAÇÃO - LANDING PAGE) - PARA FORNECEDORES TAMBÉM 


JÁ TEMOS

-LISTA DE FORNECEDORES 
-LISTA DE NOMES E CNPJS 
-SERVIDOR PARCIALMENTE CONFIGURADO 
-CODIGO DE CAPTURA DE LINKS 
-CODIGO DE CAPTURA DE PALAVRAS CHAVES DOS SITES 

FALTA 

-ARMAZENAMENTO DOS dados
-BUSCA DE DADOS 
-PROGRAMAÇÃO DA PLATAFORMA 
