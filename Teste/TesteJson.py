# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import time
import json
import io
import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoInput = path + 'ListaCNPJ_PR.csv'

start = time.clock()

dadosCNPJ = []

with open(arquivoInput, 'r') as csvFile:

    dadosCNPJ = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    
csvFile.close()

print 'Importacao: ' + str(time.clock() - start)


print dadosCNPJ[1][0]



del dadosCNPJ[0]

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'TesteArmazenamentoJson.json'

with io.open(arquivoOutput, 'w', encoding='utf-8') as f:
    
    start = time.clock()
    soma = 0
    for cnpj in dadosCNPJ:

        while time.clock() <= start:  print time.clock()

        if soma > 10:
            break
        soma += 1

        page_link = 'https://receitaws.com.br/v1/cnpj/' + str(cnpj[0])

        print(page_link)

        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

        page_response = requests.get(page_link, headers=headers)

        # print(page_response)

        page_content = BeautifulSoup(page_response.content, "html.parser")

        print(page_content)

        transform = str(page_content)

        # print transform

        result = json.loads((transform))

        # print result['uf']

        print 'Busca: ' + str(time.clock() - start)

        f.write(json.dumps(result, ensure_ascii=False))

        start = time.clock()

