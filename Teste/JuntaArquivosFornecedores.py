# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import sys  
import time
from unicodedata import normalize
reload(sys)  
sys.setdefaultencoding('utf-8')


listaFornecedores = []

start = time.clock()

# ABRE

# create directory
path = r'/Users/mateuspereiragomes/Garage/ABRE/'

arquivoInput = path + 'fornecedoresABRE.csv'

with open(arquivoInput, 'r') as csvFile:
    reader = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    print "TAM " + str(len((reader)))
    i = 0
    for row in reader:
        fornecedor = {}

        if i != 0:

            fornecedor["nome"] = row[0]
            fornecedor["endereco"] = ""
            fornecedor["telefone"] = row[4] + ' / ' + row[5]
            fornecedor["site"] = row[7]
            fornecedor["descricao"] = row[1]
            fornecedor["email"] = row[6]
            fornecedor["regiao"] = row[2]
            fornecedor["cidade"] = row[3]
            fornecedor["categoria"] = ""
            fornecedor["descricaoCategoria"] = ""
            fornecedor["planilha"] = "ABRE"
            fornecedor['codMaisPlastico'] = ""

            listaFornecedores.append(fornecedor)
        i += 1
        
csvFile.close()

print "Abre " + str(time.clock() - start)
start = time.clock()

# Mais Plastico

# create directory
path = r'/Users/mateuspereiragomes/Garage/Mais Plastico/'

arquivoInput = path + 'DadosCodigosFornecedoresMaisPlastico.csv'

with open(arquivoInput, 'r') as csvFile:
    reader = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    
    print "TAM " + str(len((reader)))
   
    i = 0 
    for row in reader:
        fornecedor = {}
        if i != 0:
            fornecedor["nome"] = row[1]
            fornecedor["endereco"] = row[2]
            fornecedor["telefone"] = row[3]
            fornecedor["site"] = row[4]
            fornecedor["descricao"] = row[5]
            fornecedor["email"] = ""
            fornecedor["regiao"] = ""
            fornecedor["cidade"] = ""
            fornecedor["categoria"] = ""
            fornecedor["descricaoCategoria"] = ""
            fornecedor["planilha"] = "MAISPLASTICO"
            cod = str(row[0]).replace('[\'fornecedores.php?codigo=','').replace('\']','')
            fornecedor['codMaisPlastico'] = cod

            listaFornecedores.append(fornecedor)
        i += 1
csvFile.close()

print "MaisPlastico " + str(time.clock() - start)
start = time.clock()

# Clube da Embalagem

# create directory
path = r'/Users/mateuspereiragomes/Garage/ClubeDaEmbalagem/'

arquivoInput = path + 'Fornecedores_ClubeDaEmbalagem.csv'

with open(arquivoInput, 'r') as csvFile:
    reader = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    print "TAM " + str(len(reader))
    
    i = 0
    for row in reader:
        fornecedor = {}
        if i != 0:

            fornecedor["nome"] = row[0]
            fornecedor["endereco"] = row[1]
            fornecedor["telefone"] = row[2]
            fornecedor["site"] = row[3]
            fornecedor["descricao"] = row[4]
            fornecedor["email"] = ""
            fornecedor["regiao"] = ""
            fornecedor["cidade"] = ""
            fornecedor["categoria"] = ""
            fornecedor["descricaoCategoria"] = ""
            fornecedor["planilha"] = "CLUBEDAEMBALAGEM"
            fornecedor['codMaisPlastico'] = ""

            listaFornecedores.append(fornecedor)
        i += 1

csvFile.close()

print "CLUBEDAEMBALAGEM " + str(time.clock() - start)
start = time.clock()


# Guia Da Embalagem

# create directory
path = r'/Users/mateuspereiragomes/Garage/GuiaDaEmbalagem/'

arquivoInput = path + 'DadosFornecedoresGuiaDaEmbalagem_Geral.csv'

with open(arquivoInput, 'r') as csvFile:
    reader = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    print "TAM " + str(len(reader))

    i = 0
    for row in reader:
        fornecedor = {}
        if i != 0:
                
            fornecedor["nome"] = row[2]
            fornecedor["endereco"] = row[3]
            fornecedor["telefone"] = row[4]
            fornecedor["site"] = row[5]
            fornecedor["descricao"] = row[7]
            fornecedor["email"] = row[6]
            fornecedor["regiao"] = ""
            fornecedor["cidade"] = ""
            fornecedor["categoria"] = row[8]
            fornecedor["descricaoCategoria"] = row[9]
            fornecedor["planilha"] = "GUIADAEMBALAGEM"
            fornecedor['codMaisPlastico'] = ""

            listaFornecedores.append(fornecedor)
        i += 1
        
csvFile.close()

print "GUIADAEMBALAGEM " + str(time.clock() - start)


# create directory
path = r'/Users/mateuspereiragomes/Garage/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'ResultadoJuncaoPlanilhasABRE-MAISPLASTICO-CLUEEGUIADAEMBALAGEM.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['Nome', 'Endereço', 'Telefone', 'Site', 'Descrição', 'Email', 'Região', 'Cidade', 'Categoria', 'DescriçãoCategoria', 'OrigemPlanilha', 'CodMaisPlastico'])
    
    for fornecedor in listaFornecedores:
        spamwriter.writerow([fornecedor["nome"],  fornecedor["endereco"] ,fornecedor["telefone"], fornecedor["site"],
         fornecedor["descricao"], fornecedor["email"], fornecedor["regiao"] , fornecedor["cidade"],fornecedor["categoria"]
         , fornecedor["descricaoCategoria"], fornecedor["planilha"], fornecedor['codMaisPlastico']  ])

