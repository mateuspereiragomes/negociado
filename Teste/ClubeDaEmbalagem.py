# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

listaFornecedor = []



page_link = 'http://www.clubedaembalagem.com.br/servicos/guia-de-fornecedores'

print(page_link)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

page_response = requests.get(page_link, headers=headers)

#print(page_response)

page_content = BeautifulSoup(page_response.content, "html.parser")

# print(page_content)

uls = page_content.find_all('ul', {"class": "item-list"})

print(len(uls))

for ul in uls:

    lis = ul.find_all("li")

    for i in range(0, len(lis)):

        nomeFornecedor = ""
        categorias = ""
        endereco = ""
        telefone = ""
        site = ""

        strongs =  lis[i].find_all('strong',{"class": "list-item-name"})

        for strong in strongs:

            nomeFornecedor = strong.text.encode("utf-8")

        ps =  lis[i].find_all('p',{"class": "list-item-category"})

        for p in ps:

            categorias = p.text.encode("utf-8")

            # categorias = re.sub('\Categorias: $', '', categorias)
            categorias = categorias.replace('Categorias: ','')

        pinfos =  lis[i].find_all('p',{"class": "list-item-info"})

        for i in range(0, len(pinfos)):

            if pinfos[i].i['class'][1] == 'fa-map-marker':
            
                endereco = pinfos[i].text.encode("utf-8")
            
            if pinfos[i].i['class'][1] == 'fa-phone':
            
                telefone = pinfos[i].text.encode("utf-8")
            
            if pinfos[i].i['class'][1] == 'fa-link':
            
                site = pinfos[i].text.encode("utf-8")
            
    
        fornecedor = []
        fornecedor.append(nomeFornecedor)
        fornecedor.append(endereco)
        fornecedor.append(telefone)
        fornecedor.append(site)
        fornecedor.append(categorias)

        listaFornecedor.append(fornecedor)

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'Fornecedores_ClubeDaEmbalagem.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['Nome', 'Endereço', 'Telefone', 'Site', 'Categorias'])
    
    for fornecedor in listaFornecedor:
        spamwriter.writerow(fornecedor)


        

    
# print("table %d" % len(tables))
