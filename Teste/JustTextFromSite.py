# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import sys  
import time
from unicodedata import normalize
reload(sys)  
sys.setdefaultencoding('utf-8')




page_link = 'http://www.tatil.com.br'

print(page_link)

headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

page_response = requests.get(page_link, headers=headers)

#print(page_response)

page_content = BeautifulSoup(page_response.content.decode('utf-8', 'ignore'), "html.parser")


data = page_content.findAll(text=True)
 
def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'video', 'img']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    return True
 
result = filter(visible, data)


mystr = ""

for resu in result:
    mystr = mystr + ' ' + resu 

print mystr.lower()

mystr = mystr.lower()

mystr = mystr.replace('\r\n', ' ')
mystr = mystr.replace('\n', ' ')
mystr = re.sub('\r?\n', ' ', mystr)
mystr = mystr.replace('.', ' ')
mystr = mystr.replace(',', ' ')
mystr = mystr.replace('!', ' ')
mystr = mystr.replace('?', ' ')

codif='utf-8'

mystr = normalize('NFKD', mystr.decode(codif)).encode('ASCII', 'ignore')

palavras = mystr.split()

def f3(seq):
   # Not order preserving
   keys = {}
   for e in seq:
       keys[e] = 1
   return keys.keys()


i = 0 
while i < len(palavras):

    palavras[i] = palavras[i].replace(' ', '')

    if palavras[i] == '' or palavras[i] == ' ':
        palavras.pop(i)
    i = i + 1

print('len pal: ' + str(len(palavras)))
novaLista =  f3(palavras)
print('len pal: ' + str(len(novaLista)))


for i in range(0, len(novaLista)):
    print novaLista[i]




def getLinks(url):

    # page_link = 'http://www.tatil.com.br'

    start = time.clock()

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(url, headers=headers)

    page_content = BeautifulSoup(page_response.content.decode('utf-8', 'ignore'), "html.parser")
   
    links = []
 
    for link in page_content.findAll('a', attrs={'href': re.compile("^http://")}):
        # if 'cadastronacional.com.br' in link.get('href'): #and link.get('href') not in links:
        links.append(link.get('href'))
    
    print('getLinks:' + str(time.clock() - start))
 
    return links

def f3(seq):
   # Not order preserving
   keys = {}
   for e in seq:
       keys[e] = 1
   return keys.keys()

links = getLinks("http://www.tatil.com.br")


# print(len(links))
# links = f3(links)
# print(len(links))

# newList = []

# soma = 1 
# for link in links:
#      newList = newList + getLinks(link)
#      print(str(soma) + '/' + str(len(links)) + ' ' +  link)
#      soma = soma + 1

# print(len(newList))
# newList = f3(newList)
# print(len(newList))

# sorted(newList, key=unicode.lower)

# soma = 1 
# for link in newList:
#     print(str(soma) + '/' + str(len(newList)) + ' ' +  link)
#     soma = soma + 1

links = ['http://www.cadastronacional.com']

startTotal = time.clock()
i = 0
while i < len(links):
    print(str(i) +  '/ ' + str(len(links)) + ' ' + links[i])
    start = time.clock()
    for link in getLinks(links[i]):
        if link not in links:   
            links.append(link)
    i = i + 1
    print('includeLinks:' + str(time.clock() - start))
    if i > 5:
        break

print('Total:' + str(time.clock() - startTotal))



# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'Links.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['Links'])
    
    for link in links:
        spamwriter.writerow([link])

