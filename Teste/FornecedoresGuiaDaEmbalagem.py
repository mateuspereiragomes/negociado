# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

id_submenu = []

for i in range(1,1000):

    pag = []

    for j in range(0,100):

        page_link = 'http://www.guiadaembalagem.com.br/sites.php?id_submenu=' + str(i) + '&pag=' + str(j)

        print(page_link)

        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

        page_response = requests.get(page_link, headers=headers)

        # print(page_response)

        page_content = BeautifulSoup(page_response.content, "html.parser")

        # print(page_content)

        uls = page_content.find_all('div', {"class": "diretorios"})

        print(len(uls))

        if len(uls) != 0:

            naoEncontrado = ""

            for ul in uls: 
                print(ul.text.encode("utf-8"))

                naoEncontrado = ul.text.encode("utf-8")

            if  'NÃO TEM REGISTROS!' in naoEncontrado:
                break
            else:
                if j == 0:
                    pag.append(i)
                else:
                    pag.append(j)
        else: 
            break

    if len(pag) != 0: 
        id_submenu.append(pag)

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'paginasValidasGuiaDaEmbalagem.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    # spamwriter.writerow(['Nome', 'Endereço', 'Telefone', 'Site', 'Categorias'])
    
    for submenu in id_submenu:
        spamwriter.writerow(submenu)


        

    
# # print("table %d" % len(tables))
