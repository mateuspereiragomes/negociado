#!/usr/bin/python
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

# create directory
path = r'dadosMarvel/'

if not os.path.isdir(path):
    os.makedirs(path)

# create lists for heroes

listaHeroes = []
listaHeroesGood = []
listaHeroesBad = []

qtdHeroesGood = 0
qtdHeroesBad = 0
qtdHeroes = 0
heroesWeight = 0
hulkIMC = 0

for hero in heroes:
    
    id = hero.getAttribute("id")
    name = hero.getElementsByTagName('name')[0].childNodes[0].data
    popularity = hero.getElementsByTagName('popularity')[0].childNodes[0].data
    alignment = hero.getElementsByTagName('alignment')[0].childNodes[0].data
    gender = hero.getElementsByTagName('gender')[0].childNodes[0].data
    height_m = hero.getElementsByTagName('height_m')[0].childNodes[0].data
    weight_kg = hero.getElementsByTagName('weight_kg')[0].childNodes[0].data
    hometown = hero.getElementsByTagName('hometown')[0].childNodes[0].data
    intelligence = hero.getElementsByTagName('intelligence')[0].childNodes[0].data
    strength = hero.getElementsByTagName('strength')[0].childNodes[0].data
    speed = hero.getElementsByTagName('speed')[0].childNodes[0].data
    durability = hero.getElementsByTagName('durability')[0].childNodes[0].data
    energy_Projection = hero.getElementsByTagName('energy_Projection')[0].childNodes[0].data
    fighting_Skills = hero.getElementsByTagName('fighting_Skills')[0].childNodes[0].data

    
    listaInterna = []
    listaInterna.append(id)
    listaInterna.append(name)
    listaInterna.append(popularity)
    listaInterna.append(alignment)
    listaInterna.append(gender)
    listaInterna.append(height_m)
    listaInterna.append(weight_kg)
    listaInterna.append(hometown)
    listaInterna.append(intelligence)
    listaInterna.append(strength)
    listaInterna.append(speed)
    listaInterna.append(durability)
    listaInterna.append(energy_Projection)
    listaInterna.append(fighting_Skills)
    
    listaHeroes.append(listaInterna)
    
    qtdHeroes = qtdHeroes + 1
    
    heroesWeight = heroesWeight + int(weight_kg)

    if alignment == "Good":
        listaHeroesGood.append(listaInterna)
        qtdHeroesGood = qtdHeroesGood + 1
    
    if alignment == "Bad":
        listaHeroesBad.append(listaInterna)
        qtdHeroesBad = qtdHeroesBad + 1

    if name == "Hulk":
        hulkIMC = float(weight_kg)/(float(height_m)*float(height_m))

# create herois.csv

arquivoOutput = path + 'herois.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['id', 'name', 'popularity', 'alignment', 'gender', 'height_m', 'weight_kg', 'hometown', 'intelligence', 'strength', 'speed', 'durability', 'energy_Projection', 'fighting_Skills'])
    
    for linha in listaHeroes:
        spamwriter.writerow(linha)



# create herois_good.csv

arquivoOutput = path + 'herois_good.csv'

with open(arquivoOutput, 'wb') as csvfile:
   
    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    spamwriter.writerow(['id', 'name', 'popularity', 'alignment', 'gender', 'height_m', 'weight_kg', 'hometown', 'intelligence', 'strength', 'speed', 'durability', 'energy_Projection', 'fighting_Skills'])

    for linha in listaHeroesGood:
        spamwriter.writerow(linha)


# create herois_bad.csv

arquivoOutput = path + 'herois_bad.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
   
    spamwriter.writerow(['id', 'name', 'popularity', 'alignment', 'gender', 'height_m', 'weight_kg', 'hometown', 'intelligence', 'strength', 'speed', 'durability', 'energy_Projection', 'fighting_Skills'])

    for linha in listaHeroesBad:
        spamwriter.writerow(linha)

# proportion of heroes good and bad
print " Proportion Good: %.2f" % (float(qtdHeroesGood*100)/float(qtdHeroes))
print " Proportion Bad: %.2f" % (float(qtdHeroesBad*100)/float(qtdHeroes))

# medium weigth
print " Media peso dos Herois: %.2f" % (float(heroesWeight)/float(qtdHeroes))


# hulk imc
print " HULK IMC: %.2f" %  hulkIMC



