# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')

listaFornecedor = []

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoInput = path + 'paginasValidasGuiaDaEmbalagem.csv'

id_submenu = []

with open(arquivoInput, 'r') as csvFile:
    reader = csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    # for row in reader:
    #     print(row)

    id_submenu = list(reader)

csvFile.close()

i = 0
j = 1 

somaTotal = 0

for id in id_submenu:

 

    for pag in range(0,len(id)):

        if pag == 0:
            i = id[0]
            j = 1 
            # if int(i) > 5:
            #     break
        else: 
            j = int(id[pag])

            somaTotal = somaTotal + 1

            if int(i) > 270 and int(i) <= 300:     
                # print('i ' + str(i) + ' j ' + str(j))   

                page_link = 'http://www.guiadaembalagem.com.br/sites.php?id_submenu=' + str(i) + '&pag=' + str(j)

                print(page_link)

                headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

                page_response = requests.get(page_link, headers=headers)

                #print(page_response)

                page_content = BeautifulSoup(page_response.content.decode('utf-8', 'ignore'), "html.parser")

                # print(page_content)

                divCategoria = page_content.find_all('div', {"style": "margin-left:-40px; position:relative; margin-top:25px; margin-bottom:50px;"})

                categoria = ""
                descricaoCategoria = ""

                for div in divCategoria:
                    categoria = div.h1.text
                    descricaoCategoria = div.h2.text


                divs = page_content.find_all('div', {"class": "cadastro "})

                # print(len(divs))

                for div in divs:

                    fornecedor = [] 

                    nomeFornecedor = ""
                    descricao = ""
                    site = ""
                    endereco = ""
                    telefone = ""
                    email = ""

                    aas = div.find_all('a', {"target": "_blank"})

                    nomeFornecedor = div.find('strong').text.encode("utf-8")
                    
                    for aa in aas:

                        site = aa['href']
                        
                    descricao = div.i.text.encode("utf-8")


                    s = div.text
                    s = s.replace('\n',' ')
                    s = s.encode('utf-8')
                    # print(s)
                    start = 'Endereço:'
                    end = 'Envie'

                    result = re.search('%s(.*)%s' % (start, end), s)

                    if result is None:
                        print(None)

                    else:

                        if result.group(1).find('Tel:') != -1:

                            enderecoTelefone = result.group(1).split('Tel:')

                            endereco = enderecoTelefone[0]

                            # endereco = endereco.encode('utf-8')

                            telefone = enderecoTelefone[1]

                            
                        
                        else:

                            endereco = result.group(1)

                            # endereco.encode('utf-8')
                    
                    

                    img = div.find('img')

                    if img != None:
                    
                        email = div.img['alt']

                        email = email.encode('utf-8')
                    

                    fornecedor.append(i)
                    fornecedor.append(j)
                    fornecedor.append(nomeFornecedor)
                    fornecedor.append(endereco)
                    fornecedor.append(telefone)
                    fornecedor.append(site)
                    fornecedor.append(email)
                    fornecedor.append(descricao)
                    fornecedor.append(categoria.encode('utf-8'))
                    fornecedor.append(descricaoCategoria.encode('utf-8'))
                

                    listaFornecedor.append(fornecedor)


# arquivoOutput = path + 'DadosFornecedoresGuiaDaEmbalagem_251-267.csv'

# with open(arquivoOutput, 'wb') as csvfile:

#     spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

#     spamwriter.writerow(['i', 'j', 'Nome', 'Endereço', 'Telefone', 'Site', 'Email', 'Descricao','Categoria', 'DescriçãoCategoria'])
    
#     for fornecedor in listaFornecedor:
#         spamwriter.writerow(fornecedor)


print(somaTotal)
print(somaTotal*20)