# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import time

import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')


# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoInput = path + 'SociosPR.txt'

start = time.clock()

listaEmpresas = []

with open(arquivoInput, 'r') as csvFile:
    reader = csv.reader(csvFile)
    soma = 0
    for row in reader:

        empresa = []
        cnpj = ""
        nomeEmpresa = ""    


        if row[0][0:2] == '01':
            cnpj = row[0][2:16]
            nomeEmpresa = row[0][16:166]

            empresa.append(cnpj)
            empresa.append(nomeEmpresa)

            listaEmpresas.append(empresa)
        soma += 1

csvFile.close()

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'ListaCNPJ_PR.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    spamwriter.writerow(['CNPJ', 'Nome'])
    
    for empresa in listaEmpresas:

        spamwriter.writerow(empresa)

print time.clock() - start