# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom
import sys  
import time
import eventlet
from unicodedata import normalize


def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title', 'video', 'img']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    return True

def f3(seq):
   # Not order preserving
   keys = {}
   for e in seq:
       keys[e] = 1
   return keys.keys()

def gravaArquivo(listaDados):
    # create directory
    path = r'Teste/'

    if not os.path.isdir(path):
        os.makedirs(path)

    arquivoOutput = path + 'ValidacaoSite2.csv'

    with open(arquivoOutput, 'w') as csvfile:
    
        spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
    
        spamwriter.writerow(['Nome', 'Endereço', 'Telefone', 'Site', 'Descrição', 'Email', 'Região', 'Cidade', 'Categoria', 'DescriçãoCategoria', 'OrigemPlanilha', 'CodMaisPlastico', 'SiteOk'])
        
        del lista[0]

        for read in lista:
            spamwriter.writerow(read)

def sitecheck(url):
    status = None
    message = ''
    headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36'}
    try:
        # with eventlet.Timeout(10):
        resp = requests.head('http://' + url, timeout=5, headers=headers)
        status = resp.status_code

    except requests.exceptions.ReadTimeout as e: 
        message = 'ReadTimeout'

    except requests.exceptions.Timeout as e: 
        message = 'Timeout'

    except requests.exceptions.ConnectTimeout as e:
        message = 'ConnectTimeout'

    except requests.ConnectionError as exc:
        # filtering DNS lookup error from other connection errors
        # (until https://github.com/shazow/urllib3/issues/1003 is resolved)

        print(exc.request)

        # if type(exc.message) != requests.packages.urllib3.exceptions.MaxRetryError:
        #     raise

        # reason = exc.message.reason    

        # if type(reason) != requests.packages.urllib3.exceptions.NewConnectionError:
        #     raise

        # if type(reason.message) != str:
        #     raise  
        
        # if ("[Errno 8] nodename nor servname " in exc.request):      # OS X
        #     message = 'DNSLookupError'
    

    return url, status, message

# create directory
path = r'/Users/mateuspereiragomes/Garage/'

arquivoInput = path + 'ResultadoJuncaoPlanilhasABRE-MAISPLASTICO-CLUEEGUIADAEMBALAGEM.csv'

lista = []

with open(arquivoInput, 'r') as csvFile:
    reader = list(csv.reader(csvFile, delimiter=';', quoting=csv.QUOTE_MINIMAL))
    lista = reader
i = 0
start = time.process_time()
for read in lista:
    # print read[3]
    if  i != 0:# and i >= 10:
        try: 
            siteOk = 0 
            print('\n')
            print(str(i) + '/' + str(len(lista)) + ' ' + read[3])
            resultado = sitecheck(read[3])
            print('--  ' +  str(resultado[1]) + ' - ' +  str(resultado[0]) + ' '  + str(resultado[2]))
            
            if (resultado[1]) == 200 or (resultado[1]) == 301 or (resultado[1]) == 302:
                siteOk = 1
            
        except ValueError:
            print(ValueError)
            print("Unexpected error:", sys.exc_info()[0])
            siteOk = 0 
        
        read.append(siteOk)
        print('Resultado =  ' + str(siteOk))
    
    if i%10==0:
        gravaArquivo(lista)
        print('\n---------')
        print(str(i) + ' ' + str(time.process_time() - start))
        print('---------\n')
        start = time.process_time()
    i += 1


gravaArquivo(lista)