# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')
 
listaCategorias = [] 

for i in range(0,56):

    # if i > 5:
    #     break

    page_link = 'https://www.maisplastico.com.br/categorias.php?pagina=' + str(i)

    print (page_link)

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    #print(page_response)

    page_content = BeautifulSoup(page_response.content, "html.parser")

    # print(page_content)

    tables = page_content.find('table', {"class": "table-hover"})

    aas = tables.find_all('a')

    for aa in aas: 

        categoria = []

        codigo = ""
        tag = "" 
        quantidade = ""

        codigo = aa['href']
        tag = aa['title']

        if aa.span != None: 
            quantidade = aa.span.text
            quantidade = quantidade.replace('produto(s)', '')

        print 'cat: ' + tag + ' qtd: ' + quantidade + ' cod: ' + codigo
     
        categoria.append(codigo)
        categoria.append(tag)
        categoria.append(quantidade)

        listaCategorias.append(categoria)


# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'CategoriasMaisPlastico.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    spamwriter.writerow(['Código', 'Categoria', 'Quantidade'])
    
    for categoria in listaCategorias:

        spamwriter.writerow(categoria)
