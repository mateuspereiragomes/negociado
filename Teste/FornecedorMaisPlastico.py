# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import os
import csv
import re
from xml.dom.minidom import parse
import xml.dom.minidom

import sys  
reload(sys)  
sys.setdefaultencoding('utf-8')

listaCodigos = []

for i in range(0,475):

    page_link = 'https://www.maisplastico.com.br/lista-fornecedores.php?pagina=' + str(i)

    print(page_link)

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    page_response = requests.get(page_link, headers=headers)

    #print(page_response)

    page_content = BeautifulSoup(page_response.content.decode('utf-8', 'ignore'), "html.parser")

    # print(page_content)

    codigos = page_content.find_all('a', {"title": "Ver Empresa"})

    for codigo in codigos:
        listaCodigos.append(codigo['href'])



def f3(seq):
   # Not order preserving
   keys = {}
   for e in seq:
       keys[e] = 1
   return keys.keys()

print('reduz')
print(len(listaCodigos))
listaCodigos = f3(listaCodigos)
print(len(listaCodigos))

# create directory
path = r'Teste/'

if not os.path.isdir(path):
    os.makedirs(path)

arquivoOutput = path + 'LinksCodigosFornecedoresMaisPlastico.csv'

with open(arquivoOutput, 'wb') as csvfile:

    spamwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)

    # spamwriter.writerow([''])
    
    for codigos in listaCodigos:

        spamwriter.writerow([codigos])
